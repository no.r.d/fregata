"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.


Simple directory browser.
"""

import nord.sigurd.managers.objectmanager as om
import pathlib
import nord.nord.utils.SpaceManager as use
import os
from panda3d.core import Point3, Vec3
import nord.sigurd.managers.containermanager as cm
from nord.sigurd.config.globalVars import PLACEDIST
import math
import nord.sigurd.shell.mainmenu as mm
from nord.sigurd.shell.menuItem import MenuItem
from nord.sigurd.utils.log import err


class FileBrowser(object):
    """The class which translate's file and directory objects into visual objects."""

    def __init__(self, startpath, app):
        """Initialize the browser and perform initial render."""
        self.startpath = pathlib.Path(pathlib.Path(startpath).resolve())
        self.app = app
        self.current_path = self.startpath
        self.offset = Vec3(1.0, -0.3, 0.3)
        self.mm_backup_items = None
        self.parent_container = None
        self.exit_callback = None

    def gen_menu(self):
        """Generate the menu items."""
        def go_parent(*args, **kwargs):
            """Render the parent directory."""
            self.current_path = self.current_path.parent
            err(f"FileBrowserPath now set to: {self.current_path}")
            self.show(first_call=False)

        mm.add_item(MenuItem("parentdir", go_parent, space="fregata"), hide_on_click=True)

        def exit_browser(*args, **kwargs):

            mm.clear_items()
            mm.restore_items(self.mm_backup_items)
            cm.enter(self.parent_container)

            if self.exit_callback is not None:
                cbk = self.exit_callback

                # collect all the files from the selection manager and pass them to the callback
                selected_files = []

                def get_selected(itm):
                    cm.sm().deselect(itm)
                    selected_files.append(itm.path)
                cm.sm().apply(get_selected)

                cbk(selected_files)
                self.exit_callback = None
            # cm.sm().pop()

        mm.add_item(MenuItem("exitcontainer", exit_browser, space="fregata"), hide_on_click=True)
        mm.reposition_items()

    def set_exit_callback(self, cbk):
        """Set the callback to call on next exit from this browser."""
        self.exit_callback = cbk

    def show(self, first_call=True):
        """Render the current path to the screen."""
        if first_call:
            self.mm_backup_items = mm.backup_items()
            mm.clear_items()
            self.gen_menu()
            self.parent_container = cm.current_name()
            # cm.sm().push()

        if not cm.has(self.current_path.resolve()):
            cm.add(self.current_path.resolve())
            cm.enter(self.current_path.resolve())

            show_cnt = 0
            pos = Point3(0.0, 0.0, 0.0)
            offset = self.offset
            n_entries = len(os.listdir(self.current_path.resolve()))
            row_elems = int(math.ceil(math.sqrt(n_entries)))
            self.offset = Vec3(0.25 * row_elems, -0.3, 0.3)

            for itm in os.listdir(self.current_path.resolve()):
                use.space("fregata")
                path = self.current_path.joinpath(itm)
                if path.is_dir():
                    module = use.module('nord.fregata.visibles.folder')
                    conname = "Folder"
                elif path.suffix == '.n':
                    module = use.module('nord.fregata.visibles.filegraph')
                    conname = "FileGraph"
                else:
                    module = use.module('nord.fregata.visibles.filegeneric')
                    conname = "FileGeneric"
                constructor = getattr(module, conname)
                visual = constructor(itm, self.app, self, path)
                # om.add_item(self.pos, self.app, f)
                show_cnt += 1
                x, y, z = pos
                x = (show_cnt % row_elems) / 4.0
                if (show_cnt % row_elems) == 0:
                    y = int(show_cnt / row_elems) / 4.0
                pos = Point3(x, y, z)
                origin = (pos - offset) * PLACEDIST
                origin = cm.cwc().get_relative_point(cm.cwc(), origin)
                om.add_item(origin, self.app, visual, mouseInteractive=False)
                visual.reparentTo(cm.cwc())

        else:
            cm.enter(self.current_path.resolve())

    def set_path(self, path):
        """Update the current path."""
        self.current_path = path
