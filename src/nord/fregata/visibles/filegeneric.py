"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
The catchall file display class for the file browser.
"""

from nord.sigurd.utils.verboser import funlog
from nord.sigurd.shell.visnode import VisNode


class FileGeneric(VisNode):
    """The class for generic filebrowser objects."""

    @funlog
    def __init__(self, uid, app, browser, path):
        """Save the path and call the parent (visnode) constructor."""
        super().__init__(uid, "filegeneric", app)
        self.path = path

    @funlog
    def new_copy_of(self):
        """Provide visNode a means of instancing me, as I want additional args..."""
        return self.__class__(self.get_new_instance_uid(), self.app, None, self.path)

    @funlog
    def update_tooltip(self, tooltip):
        """Override the Model tooltip with one that shows just the filename."""
        tooltip.set_text(f"f: ./{self.path}")  # noqa: E901
