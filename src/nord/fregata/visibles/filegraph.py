"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.


"""

from nord.sigurd.utils.verboser import funlog
from nord.sigurd.utils.log import warn
from nord.sigurd.shell.visnode import VisNode
from nord.sigurd.shell.menuItem import MenuItem
import nord.sigurd.managers.statemanager as em
import nord.sigurd.managers.event as event
import nord.sigurd.managers.containermanager as cm


class FileGraph(VisNode):
    def __init__(self, uid, app, browser, path):
        super().__init__(uid, 'filegraph', app)

        self.browser = browser
        self.path = path
        self.populated = False

        @event.callback
        @funlog
        def callback2(*args, **kwargs):
            warn("  ---> Calling Enter Graph Callback")
            em.enter_graph(self)

        self.menu.add_item(MenuItem("entercontainer", callback2))

    def enter(self):
        return
        self.browser.set_path(self.path.resolve())
        cm.enter(self.mapentry.id)
        self.browser.show()

    def exit(self):
        import vismap as Map
        Map.unload()

    def update_tooltip(self, tooltip):
        tooltip.set_text(f"*graph: {self.path}")

@event.callback
@funlog
def enter(graph):
    """Enter the folder."""
    if not graph.populated:
        cm.add(graph.uid)
    cm.enter(graph.uid)
    import vismap as Map
    Map.unload()
    Map.load(str(graph.path), graph.app)
    graph.populated = True
