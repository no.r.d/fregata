"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

Render a folder icon in the filebrowser and enable enterincg it.
"""

from nord.sigurd.utils.verboser import funlog
from nord.sigurd.utils.log import warn
from nord.sigurd.shell.visnode import VisNode
from nord.sigurd.shell.menuItem import MenuItem
import nord.sigurd.managers.statemanager as em
import nord.sigurd.managers.event as event
import nord.sigurd.managers.containermanager as cm


class Folder(VisNode):
    """The class folder."""

    def __init__(self, uid, app, browser, path):
        """Initialize the Folder."""
        super().__init__(uid, 'folder', app)
        self.browser = browser
        self.path = path
        self.populated = False

        @event.callback
        @funlog
        def callback2(*args, **kwargs):
            warn("  ---> Calling Enter Folder Callback")
            em.enter_folder(self)

        self.menu.add_item(MenuItem("entercontainer", callback2))

    def enter(self):
        return
        # self.browser.set_path(self.path.resolve())
        # cm.enter(self.uid)
        # self.browser.show()

    def exit(self):
        return

    def update_tooltip(self, tooltip):
        tooltip.set_text(f"d: {self.path.resolve()}/")

    @funlog
    def new_copy_of(self):
        """Provide visNode a means of instancing me, as I want additional args..."""
        return self.__class__(self.get_new_instance_uid(), self.app, self.browser, self.path)


@event.callback
@funlog
def enter(folder):
    """Enter the folder."""
    folder.browser.set_path(folder.path.resolve())
    if not folder.populated:
        cm.add(folder.uid)
    cm.enter(folder.uid)
    if not folder.populated:
        folder.browser.show(first_call=False)
        folder.populated = True
